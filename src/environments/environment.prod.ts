export const environment = {
  production: true,
  googleBooksApi: 'https://www.googleapis.com/books/v1/volumes'
};
