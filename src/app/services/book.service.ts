import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  public searchBookByName(query) {
    return this.http.get(`${environment.googleBooksApi}?q=${query}`).toPromise();
  }
}
