import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    console.log('AuthGuardService: canActivate was called');
      const isAuth: boolean = localStorage.getItem('isAuth') === 'true';
      if (!isAuth) {
        this.router.navigate(['login']);
      }
      return isAuth;
  }
}
