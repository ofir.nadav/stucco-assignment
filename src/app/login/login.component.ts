import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'stu-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isAuth: boolean = localStorage.getItem('isAuth') === 'true';
  userName: string = localStorage.getItem('userName');
  wishlist: any[] = JSON.parse(localStorage.getItem('wishlist'));

  constructor(private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.userName = this.userName ? this.userName : 'Guest';
    this.wishlist = this.wishlist && this.wishlist.length ? this.wishlist : [];
    this.loginForm = new FormGroup({
      name: new FormControl(this.userName, [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$')
      ])
    });
  }

  submit(loginForm) {
    if (!this.loginForm.valid) {
      this.markFormInvalid(loginForm);
    } else {
      const user = loginForm.value;
      this.login(user);
    }
  }

  markFormInvalid(formObject: FormGroup | any) {
    Object.keys(formObject.controls).forEach(key => {
      const ctrl = formObject.get(key);
      if (ctrl.value && ctrl.controls) {
        this.markFormInvalid(formObject.controls[key]);
      }
      if (!ctrl.valid) {
        console.log(`markFormInvalid: ${key} is not valid`);
        return ctrl.markAsTouched();
      }
    });
  }

  login(user) {
    localStorage.setItem('isAuth', 'true');
    localStorage.setItem('userName', user.name);
    localStorage.setItem('wishlist', JSON.stringify(this.wishlist));
    this.router.navigate(['']);
  }

}
