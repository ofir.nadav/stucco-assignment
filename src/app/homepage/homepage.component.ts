import { BookService } from './../services/book.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'stu-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  isAuth: boolean = localStorage.getItem('isAuth') === 'true';
  userName: string = localStorage.getItem('userName');
  wishlist: string[] = JSON.parse(localStorage.getItem('wishlist'));
  bookList: any[];
  bookModal: any;

  constructor(private bookService: BookService) { }

  ngOnInit() {
    if (this.isAuth) {
      this.userName = this.userName ? this.userName : 'Guest';
      this.wishlist = this.wishlist && this.wishlist.length ? this.wishlist : [];
    } else {
      // TODO: redirect to login
    }
  }

  async bookSearch(query) {
    // Query must be at least 2 characters.
    if (query.length >= 2) {
      const res: any = await this.bookService.searchBookByName(query);
      this.bookList = res.items;
    } else {
      this.bookList = [];
    }
  }

  isInWishlist(book) {
    let inWishlist = false;
    if (book) {
      inWishlist = this.wishlist.includes(book.id);
    }
    return inWishlist;
  }

  showDialog(book) {
    this.bookModal = book;
  }

  addToWishlist(book) {
    this.wishlist = this.wishlist || [];
    this.wishlist.push(book.id);
    localStorage.setItem('wishlist', JSON.stringify(this.wishlist));
  }

}
