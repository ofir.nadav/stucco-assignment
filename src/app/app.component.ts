import { Component } from '@angular/core';

@Component({
  selector: 'stu-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Stucco Assignment';
  isAuth = localStorage.getItem('isAuth');
}
